
// Truncate card title 
window.addEventListener("load",function(){
    truncateCardTitle();
})

function truncateCardTitle(){
    var cardList = document.getElementsByClassName("card-title");
    console.log(cardList);
    for(var i = 0; i < cardList.length;i++){
        var text = cardList[i].innerHTML;
        var newText = truncateString(text,30);
        cardList[i].innerHTML = newText;
    }
}
function truncateString(str,num){
    if(str.length > num){
        return str.slice(0,num) + "...";
    }
    else{
        return str;
    }
}
// sidebarmini
var toogleBtn = document.querySelector('.sidebar__mini__btn');
var sidebarMini = document.querySelector('.sidebar__mini');
var switchBtn = document.querySelector('#checkbox');
toogleBtn.addEventListener('click',function(){
    sidebarMini.classList.toggle('is-opened');
});
switchBtn.addEventListener('click',function(){
    document.querySelector('body').classList.toggle('darkMode');
});